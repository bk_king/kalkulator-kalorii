(function () {
    var inputWeight = {
        element: document.querySelector('input[name=weight]')

    },
    buttonsActivity = {
        element: document.querySelectorAll('button[data-name]'),
        getElement: function (name) {
            var i, length = buttonsActivity.element.length;
            for (i = 0; i < length; i++) {
                if (buttonsActivity.element[i] === name) {
                    return buttonsActivity.element[i];
                }
            }
            return false;
        },
        //button wymaga data-name atrybutu poniewaz jest wyszukiwany po nim
        getDataAttribute: function (name, dataAttribute) {
            var element = buttonsActivity.getElement(name);
            if (element !== false) {
                for (var key in element.dataset) {
                    if (key === dataAttribute) {
                        return element.dataset[key];
                    }
                }
            }
            return false;
        }
    },
    modalButtons = {
        element: document.querySelectorAll('button[data-input]'),
        getElement: function (name) {
            var i, length = modalButtons.element.length;
            for (i = 0; i < length; i++) {
                if (modalButtons.element[i] === name) {
                    return modalButtons.element[i];
                }
            }
            return false;
        },
        getDataAttribute: function (input, dataAttribute) {
            var element = modalButtons.getElement(input);
            if (element !== false) {
                for (var key in element.dataset) {
                    if (key === dataAttribute) {
                        return element.dataset[key];
                    }
                }
            }
            return false;
        }
    },
    trElements = {
        element: document.querySelectorAll('table tbody tr'),
        aerobicElement: document.querySelectorAll('div.modal_aerobic table tbody tr'),
        anaerobicElement: document.querySelectorAll('div.modal_anaerobic table tbody tr'),
        getDataAttributeFromTd: function (element, dataAttribute) {
            if (element !== undefined) {
                var length = element.children.length;

                for (var key in element.children[length - 1].dataset) {
                    if (key === dataAttribute) {
                        return element.children[length - 1].dataset[key];
                    }
                }
            }
            return false;
        },
        isSetClass: function (elements, className) {
            var i, length = elements.length;
            for (i = 0; i < length; i++) {
                if (elements[i].className === className) {

                    return elements[i];
                }
            }
            return false;
        }
    },
    modalInputs = {
        element: document.querySelectorAll('div.modal div.row div.form-group input'),
        getElement: function (id) {

            if (modalInputs.element.length > 0) {
                var i, j, length = modalInputs.element.length;
                for (i = 0; i < length; i++) {
                    if (modalInputs.element[i].getAttribute('id') === id) {
                        return modalInputs.element[i];
                    }
                }
            }
            return false;
        },
        getElementsByData: function (data) {
            var i, inputs = [], length = modalInputs.element.length;
            for (i = 0; i < length; i++) {
                for (var key in modalInputs.element[i].dataset) {
                    if (modalInputs.element[i].dataset[key] === data) {
                        inputs.push(modalInputs.element[i]);
                    }
                }
            }
            return inputs;
        }
    };
    previous = null;
    function init() {
        
        window.myAppAddEventInLoop(buttonsActivity.element, getValue, 'click');
        window.myAppAddEventInLoop(trElements.element, setClass, 'click');
        window.myAppAddEventInLoop(modalButtons.element, initValidation, 'click');

    }
    var flagObject = {
        'anaerobic-weightFlag': false,
        'aerobic-weightFlag': false
    };
    function initValidation(e) {
        var dataInput = e.target.dataset.input,
                inputs = modalInputs.getElementsByData(dataInput);
        var object = window.myApp(inputs);
        var everyCorr = object.corrText.concat(object.corrRadio);
        var everyErr = object.errText.concat(object.errRadio);
        var th = document.querySelectorAll('div.modal_' + dataInput + ' table thead tr')[1].firstElementChild;
        var trValidation = trElements.isSetClass(trElements[dataInput + 'Element'], 'success');
        window.myAppClass(object.errText, 'correct', 'error');
        window.myAppWarrningsRemove(everyErr, 'hidden');
        window.myAppClass(object.corrText, 'error', 'correct');
        window.myAppWarrningsAdd(everyCorr, 'hidden');
        if (everyErr.length === 0 && trValidation) {
            e.target.setAttribute('data-dismiss', 'modal');
            th.classList.add('hidden');
            setValue(dataInput);
        } else {
            e.target.setAttribute('data-dismiss', '');
        }
        if (trValidation) {
            th.classList.add('hidden');
        } else {
            th.classList.remove('hidden');
        }
    }
    function setClass(e) {
        var target = e.target;
        removeClass(previous);
        target.parentElement.classList.add('success');
        previous = target.parentElement;
    }
    function setValue(data) {
        var inputs = modalInputs.getElementsByData(data),
                activity = document.querySelector('input[name=' + data + ']'),
                factor = trElements.getDataAttributeFromTd(previous, 'factor');
        activity.classList.remove('error');
        activity.classList.add('correct');
        window.myAppWarrningsAdd([activity], 'hidden');
        activity.value = Math.round((factor * inputs[0].value) * (inputs[1].value / 60));
    }
    function removeClass(previous) {
        if (previous !== null) {
            previous.classList.remove('success');
        }
    }
    function getValue(e) {
        var dataName = buttonsActivity.getDataAttribute(e.target, 'name');
        var splitArray = dataName.split('-');
        var inputsModal = modalInputs.getElementsByData(splitArray[0]);
        var inputElement = modalInputs.getElement(dataName);

        if (flagObject[dataName + 'Flag'] === false) {
            window.myAppWarrnings(inputsModal, 't00ltip hidden');
            flagObject[dataName + 'Flag'] = true;
        }

        if (!isNaN(inputWeight.element.value) && inputWeight.element.value !== '') {
            inputElement.classList.remove('error');
            window.myAppWarrningsAdd([inputElement], 'hidden');
            inputElement.classList.add('correct');
            inputElement.value = inputWeight.element.value;
        } else {
            inputElement.value = '';
            inputElement.classList.remove('error');
            window.myAppWarrningsAdd([inputElement], 'hidden');
            inputElement.classList.remove('correct');
        }

    }
//    function addEventsInLoop(elements, functionName, event) {
//        var length = elements.length, i;
//        for (i = 0; i < length; i++) {
//            addEvent(elements[i], event, functionName);
//        }
//    }
//    function addEvent(element, event, functionName) {
//        element.addEventListener(event, functionName);
//    }
    init();
}());

