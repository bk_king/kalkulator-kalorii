(function () {

    var calc = {
        tooltips: document.querySelectorAll('label[data-toggle=tooltip]'),
        inputs: {
            element: document.querySelectorAll('input[data-calc]'),
            getElementsByData: function (data, value) {
                var i, inputs = [], length = calc.inputs.element.length;
                for (i = 0; i < length; i++) {
                    for (var key in calc.inputs.element[i].dataset) {
                        if (calc.inputs.element[i].dataset[key] === value && key === data) {
                            inputs.push(calc.inputs.element[i]);
                        }
                    }
                }
                return inputs;
            },
            elementsToValidate : [],
            getAllToValidateInputs: function (elements) {

                var i, length = elements.length, currentName = '',
                        validationResult = [];

                for (i = 0; i < length; i++) {

                    if (elements[i].type === 'radio') {
                        if (currentName === elements[i].name) {
                            continue;
                        }
                        currentName = elements[i].name;
                    }
                    validationResult.push(elements[i]);

                }
               calc.inputs.elementsToValidate = validationResult;
                return validationResult;
            }

        },
        init: function () {
            calc.createWarrning(calc.inputs.getAllToValidateInputs(calc.inputs.element), 't00ltip hidden');
            var button = document.querySelector('#btn');
            button.addEventListener('click', calc.startAplication);
            var inputsText = calc.inputs.getElementsByData('calc', 'text');
            var inputsRadio = calc.inputs.getElementsByData('calc', 'radio');
            calc.addEventsInLoop(inputsText, calc.validationText, 'blur');
            calc.addEventsInLoop(inputsRadio, calc.validationRadio, 'click');
        },
        checkWidth: function(){
           
            console.log(document.querySelectorAll('div.t00ltip'));
            calc.changeView(document.querySelectorAll('div.t00ltip'), 'hidden');
        },
        addEventsInLoop: function (elements, functionName, event) {
            var length = elements.length, i;
            for (i = 0; i < length; i++) {
                calc.addEvent(elements[i], event, functionName);
            }
        },
        
        addEvent: function (element, event, functionName) {
            element.addEventListener(event, functionName);
        },
        validationText: function (e) {
            if (calc.validationTextInputs(this)) {
                this.classList.remove('error');
                calc.addWarrningsClass([this], 'hidden');
                this.classList.add('correct');
            } else {
                this.classList.remove('correct');
                calc.removeWarrningsClass([this], 'hidden');
                this.classList.add('error');
            }
        },
        validationRadio : function (e){
             if (!calc.validateRadio([this])) {
                calc.addWarrningsClass([this], 'hidden');
            } 
        },
        parts: {},
        startAplication: function (e) {
            var validationElements = calc.validation(calc.inputs.elementsToValidate);
            var everyCorr = validationElements.corrText.concat(validationElements.corrRadio);
            var everyErr = validationElements.errText.concat(validationElements.errRadio);
            calc.changeElementsClass(validationElements.errText, 'correct', 'error');
            calc.removeWarrningsClass(everyErr, 'hidden');
            calc.changeElementsClass(validationElements.corrText, 'error', 'correct');
            calc.addWarrningsClass(everyCorr, 'hidden');
            if (everyErr.length === 0) {
                var formData = calc.objectFormValues(everyCorr, 'name');
                e.target.setAttribute('data-target', '.myResult');
                calc.getResult(formData);
            }else{
                e.target.removeAttribute('data-target');
            }

        },
        removeWarrningsClass: function (elements, className) {
            var i, length = elements.length;
            for (i = 0; i < length; i++) {

                if (elements[i].type === "text") {
                    elements[i].parentElement.lastChild.classList.remove(className);
                } else if (elements[i].type === "radio") {
                    elements[i].parentElement.parentElement.lastChild.classList.remove(className);
                }
            }
        },
        addWarrningsClass: function (elements, className) {
            var i, length = elements.length;
            for (i = 0; i < length; i++) {

                if (elements[i].type === "text") {
                    elements[i].parentElement.lastChild.classList.add(className);
                } else if (elements[i].type === "radio") {
                    elements[i].parentElement.parentElement.lastChild.classList.add(className);
                }
            }
        },
        createWarrning: function (elements, className) {
            var i, length = elements.length;
            for (i = 0; i < length; i++) {
                var warrningElement = document.createElement('div');
                warrningElement.setAttribute('class', className);
                warrningElement.textContent = elements[i].dataset.error;

                if (elements[i].type === "text") {
                    elements[i].parentElement.appendChild(warrningElement);
                } else if (elements[i].type === "radio") {
                    elements[i].parentElement.parentElement.appendChild(warrningElement);
                }
            }
        },
        changeElementsClass: function (elements, toDelete, toSet) {
            var i, length = elements.length;
            for (i = 0; i < length; i++) {
                elements[i].classList.remove(toDelete);
                elements[i].classList.add(toSet);
            }
        },
        objectFormValues: function (elements, attribute) {
            var i, length = elements.length, obj = {};

            for (i = 0; i < length; i++) {
                obj[elements[i].getAttribute(attribute)] = elements[i].value;
            }
            return obj;


        },
        validation: function (elements) {
            var i, length = elements.length, flag, currentName = '',
                    validationResult = {
                        errText: [],
                        corrText: [],
                        corrRadio: [],
                        errRadio: [],
                        everyElements: []
                    };
            for (i = 0; i < length; i++) {
                flag = true;
                if (elements[i].type === 'text') {
                    flag = calc.validationTextInputs(elements[i]);
                    if (flag === true) {
                        validationResult.corrText.push(elements[i]);
                    } else {
                        validationResult.errText.push(elements[i]);
                    }
                } else if (elements[i].type === 'radio') {
                    if (currentName === elements[i].name) {
                        continue;
                    }
                    flag = calc.validateRadio(elements[i]);
                    currentName = elements[i].name;
                    if (flag === false) {
                        validationResult.errRadio.push(elements[i]);
                    } else {
                        validationResult.corrRadio.push(flag);
                    }
                }
                validationResult.everyElements.push(elements[i]);

            }
            return validationResult;
        },
        validateRadio: function (target) {
            var nameAttr = target.name,
                    radioInputs = document.querySelectorAll('input[name=' + nameAttr + ']'),
                    length = radioInputs.length, i;
            for (i = 0; i < length; i++) {
                if (radioInputs[i].checked === true) {
                    return radioInputs[i];
                }
            }
            return false;
        },
        getResult: function (object) {
            var result = 0, sex, tef, neat, tea, epoc;
            if (object.sex === 'female') {
                sex = (9.99 * object.weight) + (6.25 * object.tall) - (4.92 * object.age) - 161;
            } else if (object.sex === 'male') {
                sex = (9.99 * object.weight) + (6.25 * object.tall) - (4.92 * object.age) + 5;
            }
            tef = sex * 0.1;
            neat = sex * object.neat;
            tea = (parseInt(object.anaerobic) + parseInt(object.aerobic)) * object.days / 7;
            epoc = sex * object.epoc;
            result = sex + tef + neat + tea + epoc;
            if (object.phase === 'bulk') {
                result += result * 0.15;
            } else if (object.phase === 'reduce') {
                result -= result * 0.15;
            }
            result = Math.round(result);

            calc.createResult(result, '#result');

        },
        createResult: function (result, selector) {
            var element = document.querySelector(selector);
            element.textContent = result + ' kcal, powodzenia!';
        },
        validationTextInputs: function (input) {
            if (isNaN(input.value)) {
                return false;
            }
            return input.value !== '';
        }
    };
    window.myApp = calc.validation;
    window.myAppClass = calc.changeElementsClass;
    window.myAppWarrnings = calc.createWarrning;
    window.myAppWarrningsRemove = calc.removeWarrningsClass;
    window.myAppWarrningsAdd = calc.addWarrningsClass;
    window.myAppWarrningsAdd = calc.addWarrningsClass;
    window.myAppAddEventInLoop = calc.addEventsInLoop;
    calc.init();

})();

